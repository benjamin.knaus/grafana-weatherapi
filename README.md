# Description

Grafana dashboard to visualize data from weatherapi.com:

![WeatherAPI](images/WeatherAPI.png)

# Configuration

Please put your API key in dashboard variable "key".

# Export

```
$ curl http://localhost:3000/api/datasources/4|python -m json.tool > datasource.json
$ curl http://localhost:3000/api/dashboards/uid/e1bc5fd1-491d-4b3f-9e15-9e5f84e2d3df|python -m json.tool > dashboard.json
```
